var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://localhost:27017/my_db_name';
//Данные для  коллекции
var users = [
    {
        name: 'Alex',
        gender: 'm'
    },
    {
        name: 'Petr',
        gender: 'm'
    },
    {
        name:'Anna',
        gender:'f',
    },
    {
        name:'Olga',
        gender:'f',
    }
];

MongoClient.connect(url , (err,db)=>{
    if (err) {
        console.log('Невозможно подключиться к серверу MongoDB. Ошибка: ', err);
    }
    else {
        console.log('Соединение установлено для ', url);
        
        var collection = db.collection('users');//создание коллекции
        collection.insert(users, (err,result) =>{//добавление данных в коллекцию
            if(err){
                console.log('В коллекцию не добавлены документы, ошибка', err);
            }
            else{//в случае успешного добавления выполняем следующее
                collection.find({}, {_id:0 }).toArray( (err,result)=> {//поиск в коллекции всех данных и их вывод без id
                    if (err){
                         console.log('Ошибка: ', url);
                    }
                    else if (result.length){
                        console.log('Результаты поиска', result);    
                    }
                    else {
                        console.log('Ни чего не найдено');
                    }
                });
                //обновление поля name у документов где gender равно 2
                collection.update({gender:"f"}, {$set: {name:"Jessica"}}, {multi:true}, (err, result)=> {
                    if (err){
                         console.log('Ошибка добавления: ', url);
                    }
                    else {
                        console.log('Изменины: ', result.result.nModified);// колличесво изминенных
                    }
                });
                //обновление с помощью updateMany
                collection.updateMany({gender:"m"}, {$set: {name:"Sergey"}}, (err, result)=> {
                    if (err){
                         console.log('Ошибка добавления: ', url);
                    }
                    else {
                        console.log('Изменины: ', result.result.nModified);// колличесво изминенных
                    }
                });
                
                collection.find({name:"Sergey",gender:"m"}, {_id:0 }).toArray( (err,result)=> {//вывод документов
                    if (err){
                         console.log('Ошибка: ', url);
                    }
                    else if (result.length){
                        console.log('Измененные документы', result);
                    }
                    else {
                        console.log('Ни чего не найдено');
                    }
                });
                
                collection.find({name:"Jessica",gender:"f"}, {_id:1 }).toArray( (err,result)=> {//вывод id измененных документов
                    if (err){
                         console.log('Ошибка: ', url);
                    }
                    else if (result.length){
                        console.log('Измененные документы', result);
                    }
                    else {
                        console.log('Ни чего не найдено');
                    }
                });
                
                collection.remove( (err, numberOfRemovedDocs) => {// удаление всех данных с указанием их колличества
                    if (err){
                        console.log('Ошибка удаления: ', err);
                    }
                    else {
                        console.log('Удалено документов: ', numberOfRemovedDocs.result.n);
                    }
                });
            }
            db.close();//закрытие соединения
        });
    }
});